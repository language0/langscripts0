local util = require('mp.utils')
local mpopt = require('mp.options')

local options = {
    media_path = '/home/stegatxins0/.local/share/Anki2/User 1/collection.media/',
    deckname = '🍕日本語::🥇日本語',
    modelName = 'MIA Jpn DX',
    tagName = 'mia2anki',
    field_audio = 'Sentence Audio',
    field_snapshot = 'Image',
    field_subtitle = 'Sentence',
    anki_url = 'localhost:8765',
    audio_bitrate = '128k',
    snapshot_height = '480',
}

mpopt.read_options(options)

local ctx = {
    start_time = -1,
    end_time = -1,
    snapshot_time = -1,
    sub = {''},
}

-- local function time_to_string(seconds)
--     if seconds < 0 then
--         return 'empty'
--     end
--     local time = string.format('.%03d', seconds * 1000 % 1000);
--     time = string.format('%02d:%02d%s',
--         seconds / 60 % 60,
--         seconds % 60,
--         time)
--
--     if seconds > 3600 then
--         time = string.format('%02d:%s', seconds / 3600, time)
--     end
--
--     return time
-- end

local function time_to_string2(seconds)
    return string.format('%02dh%02dM%02ds%03dm',
        seconds / 3600,
        seconds / 60 % 60,
        seconds % 60,
        seconds * 1000 % 1000)
end

local function anki_connect(action, t_params, url)
    local url = url or 'localhost:8765'

    local request = {
        action = action,
        version = 6,
    }

    if t_params ~= nil then
        request.params = t_params
    end

    local json = util.format_json(request)

    local command = {
        'curl', url, '-X', 'POST', '-d', json
    }

    local result = mp.command_native({
        name = 'subprocess',
        args = command,
        capture_stdout = true,
        capture_stderr = true,
    })

    result = util.parse_json(result.stdout)
	if result == nil then
		mp.osd_message("✘: Anki not found")
	elseif result.result == nil and result.error then
		mp.osd_message("✘: "..result.error)
	elseif result.result and result.error ==nil then
		mp.osd_message("✔")
	end
end

function create_anki_note(gui)

    local filename_prefix = generate_media_name()

    local filename_audio = create_audio(
        options.media_path,
        filename_prefix,
        ctx.start_time,
        ctx.end_time,
        options.audio_bitrate)

    local filename_snapshot = create_snapshot(
        options.media_path,
        filename_prefix,
        ctx.snapshot_time,
        options.snapshot_height
    )

    -- Start filling the fields
    local fields = {}

    if #filename_audio > 0 then
        fields[options.field_audio] = '[sound:'..filename_audio..']'
    end

    if #filename_snapshot > 0 then
        fields[options.field_snapshot] = '<img src="' .. filename_snapshot ..'">'
    end

    if #ctx.sub > 0 then
        fields[options.field_subtitle] = ctx.sub
    end

	local param = {
        note = {
            deckName = options.deckname,
            modelName = options.modelName,
            fields = fields,
            tags = {
               options.tagName
            }

        }
    }

    local action;

    if gui then
        action = 'guiAddCards'
    else
        action = 'addNote'
    end

    anki_connect(action, param, options.anki_url)
end
function generate_media_name()
    local name = mp.get_property('filename/no-ext')
    name = string.gsub(name, '[%[%]]', '')
    return 'mia2anki_' .. name
end

function create_audio(
    media_path,
    filename_prefix,
    start_time,
    end_time,
    bitrate)

    if (start_time < 0 or end_time < 0) or
       (start_time == end_time) then
        return ''
    end

    if start_time > end_time then
        local t = start_time
        start_time = end_time
        end_time = t
    end

    local filename = string.format(
        '%s_(%s-%s).mp3',
        filename_prefix,
        time_to_string2(start_time),
        time_to_string2(end_time))

    local encode_args = {
        'mpv', mp.get_property('path'),
        '--start=' .. start_time,
        '--end=' .. end_time,
	'--aid=' .. mp.get_property("aid"),
        '--vid=no',
        '--loop-file=no',
        '--oacopts=b=' .. bitrate,
        '-o=' .. media_path .. filename
    }

    local result = mp.command_native({
        name = 'subprocess',
        args = encode_args,
        capture_stdout = true,
        capture_stderr = true,
    })

    return filename
end

function create_snapshot(media_path, filename_prefix, snapshot_time, height)
    if (snapshot_time <= 0) then
        return ''
    end

    local filename = string.format(
        '%s_(%s).jpg',
        filename_prefix,
        time_to_string2(snapshot_time)
    )

    local encode_args = {
        'mpv', mp.get_property('path'),
        '-start=' .. snapshot_time,
        '--frames=1',
        '--no-audio',
        '--no-sub',
        '--vf-add=scale=-2:' .. height,
        '--ovcopts=global_quality=2*QP2LAMBDA,flags=+qscale',
        '--loop-file=no',
        '-o=' .. media_path .. filename
    }

    local result = mp.command_native({
        name = 'subprocess',
        args = encode_args,
        capture_stdout = true,
        capture_stderr = true,
    })

    return filename
end

function set_time_to_subs()
	local sub_delay = mp.get_property_native("sub-delay")
	local start_time = mp.get_property_number('sub-start') + sub_delay
	local end_time = mp.get_property_number('sub-end') + sub_delay

	if ctx.start_time == start_time and ctx.end_time == end_time then
		ctx.start_time = -1
		ctx.end_time = -1
	else
		ctx.start_time = start_time
		ctx.end_time = end_time
	end
end

function set_time(property)
    local time = mp.get_property_number('time-pos')
    if time == ctx[property] then
        ctx[property] = -1
    else
        ctx[property] = time
    end
end

function set_subs()
    local subs = mp.get_property('sub-text'):gsub('\n', '')

    if subs == ctx.sub then
        ctx.sub = ''
    else
        ctx.sub = subs
    end
end

function addankicard()
	set_time('snapshot_time')
	set_time_to_subs()
	set_subs()
	create_anki_note(false)
end

mp.add_key_binding("x", "mia2anki", addankicard)
