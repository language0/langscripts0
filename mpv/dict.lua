local script_dir = '/home/stegatxins0/zero/dev/mard0/v1/main.py'
local mard0_get = "python ".. script_dir ..' -o ass -p "'
local ov = mp.create_osd_overlay("ass-events")
local running
local function get_sub(name, value)
	if running and type(value) == 'string' then
		if pcall(ov) == true then
		    ov.remove()
		end

 		local handle = io.popen(mard0_get .. value .. '"')
 		local result = handle:read("*a")
 		handle:close()
		ov.data = result
		--ov.data = "{\\c&H00cccc>&}hello world"
		ov:update()
	end
end


local function start_stop()
	if running then
		mp.msg.warn('Quitting dict ...')
		running = false
		if pcall(ov) == true then
		    ov.remove()
		end
		mp.unobserve_property("sub-text", get_sub)
	else
			running = true
			mp.msg.warn('Starting dict ...')
			mp.observe_property("sub-text", 'string', get_sub)
	end
end

mp.add_key_binding("y", "start-stop-dict", start_stop)
