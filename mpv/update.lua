-- ----------------------------------------
--   ___  ____ _____ ___ ___  _   _ ____  
--  / _ \|  _ \_   _|_ _/ _ \| \ | / ___| 
-- | | | | |_) || |  | | | | |  \| \___ \ 
-- | |_| |  __/ | |  | | |_| | |\  |___) |
--  \___/|_|    |_| |___\___/|_| \_|____/ 
-- ----------------------------------------
--  My own workflow: save this to unwatched path and create a mpv script to move to watched path
--  Path to unfinished condensed audio (file to be moved from)

condensedpath='/home/stegatxins0/zero/日本語/オーディオ/未完成/'
--  Path to finished condensed audio (file to be moved to)
finishedpath='/home/stegatxins0/zero/日本語/オーディオ/完成/'
--  Path to list to store anime
listfilepath='/home/stegatxins0/zero/アニメ/animerecord.txt'
-- ----------------------------------------
-- ----------------------------------------

function is_url(path)
  if path ~= nil and string.sub(path,1,4) == "http" then
    return true
  else
    return false
  end
end
function findfilename()
  local t={}
  for str in string.gmatch(dir, "([^"..'/'.."]+)") do
    table.insert(t, str)
  end
  seriesName = t[#t-1]
  filename_withext = t[#t]
  filename_withoutext = filename_withext:match("(.+)%..+")
end

function condenseupdate()
  --local create_folder = "[ ! -d " .. finishedpath .. seriesName .. "] && mkdir " .. finishedpath .. seriesName
  local movecmd = "mv '" .. condensedpath .. seriesName .. "/" .. filename_withoutext .. ".condensed.mp3' '" .. finishedpath .. seriesName .. "_" .. filename_withoutext .. ".mp3'"
  os.execute(movecmd)
	mp.msg.warn('Condensed audio copied')
end

function addtolist()
  local out = io.open(listfilepath,'a')
  local date = os.date("%c")
  local record = date .. "  " .. seriesName .. "    " .. filename_withoutext
  out:write("\n" .. record)
  io.close(out)
	mp.msg.warn('Added to list')
end

mp.register_script_message("locate-current-file", function()
  local path = mp.get_property("path")
  dir = mp.get_property("working-directory") .. '/' .. path
  if path ~= nil then
    if is_url(path) then
      mp.osd_message("URL detected, current version do not support URL. No actions will be taken.")
    else
      findfilename()
      condenseupdate()
      addtolist()
    end
  else
    mp.osd_message("'path' property was empty, no media has been loaded.")
  end
end)
