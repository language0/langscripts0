Read [CONTRIBUTING.md](CONTRIBUTING.md) before you submit this pull request:

Reading that and following the rules will get your pull request reviewed and merged faster. Nobody wants lazy pull requests.
